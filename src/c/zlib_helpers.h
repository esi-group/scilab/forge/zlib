#ifndef __ZLIB_HELPERS_H__
#define __ZLIB_HELPERS_H__

#include <wchar.h>

#define FILE_OPEN_READ_ERROR -1000
#define FILE_OPEN_WRITE_ERROR -2000

int zlibcompressfilename(const wchar_t *wcSource, const wchar_t *wcDestination, int level);
int zlibdecompressfilename(const wchar_t *wcSource, const wchar_t *wcDestination);

char *zlibcompress(const char *strToCompress, int level, int *iErr);
char *zlibdecompress(const char *strToDecompress, int *iErr);

char *zlibmessagerror(int ret);

#endif /* __ZLIB_HELPERS_H__ */
