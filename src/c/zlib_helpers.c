/* ==================================================================== */
/* Based on http://www.zlib.net/zpipe.c */
/* Allan CORNET - 2011 */
/* ==================================================================== */
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "zlib.h"
#include "MALLOC.h"
#include "localization.h"
#include "zlib_helpers.h"
#include "charEncoding.h"
#include "stack-def.h"
/* ==================================================================== */
#if defined _MSC_VER
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif
/* ==================================================================== */
#define CHUNK 16384
/* ==================================================================== */
static int zlibcompressfile(FILE *source, FILE *dest, int level);
static int zlibdecompressfile(FILE *source, FILE *dest);
/* ==================================================================== */
/* Compress from file source to file dest until EOF on source.
def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
allocated for processing, Z_STREAM_ERROR if an invalid compression
level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
version of the library linked do not match, or Z_ERRNO if there is
an error reading or writing the files. */
static int zlibcompressfile(FILE *source, FILE *dest, int level)
{
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK) return ret;

    /* compress until end of file */
    do 
    {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) 
        {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
        compression if all of source has been read in */
        do 
        {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) 
            {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}
/* ==================================================================== */
/* Decompress from file source to file dest until stream ends or EOF.
inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
allocated for processing, Z_DATA_ERROR if the deflate data is
invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
the version of the library linked do not match, or Z_ERRNO if there
is an error reading or writing the files. */
static int zlibdecompressfile(FILE *source, FILE *dest)
{
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do 
    {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) 
        {
            (void)inflateEnd(&strm);
            return Z_ERRNO;
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do 
        {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) 
            {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) 
            {
                (void)inflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}
/* ==================================================================== */
char *zlibmessagerror(int ret)
{
    char *msg = (char *)MALLOC(sizeof(char) * 1024);
    if (msg)
    {
        switch (ret)
        {
        case Z_ERRNO: 
            break;
        case Z_STREAM_ERROR:
            sprintf(msg, _("invalid compression level"));
            break;
        case Z_DATA_ERROR:
            sprintf(msg, _("invalid or incomplete deflate data"));
            break;
        case Z_MEM_ERROR: case Z_BUF_ERROR:
            sprintf(msg,  _("out of memory"));
            break;
        case Z_VERSION_ERROR:
            sprintf(msg, _("zlib version mismatch"));
            break;
        case FILE_OPEN_READ_ERROR:
            sprintf(msg, _("can not read file"));
            break;
        case FILE_OPEN_WRITE_ERROR:
            sprintf(msg, _("can not write file"));
            break;
        }
    }
    return msg;
}
/* ==================================================================== */
int zlibcompressfilename(const wchar_t *wcSource, const wchar_t *wcDestination, int level)
{
    int iErr = Z_OK;
    FILE *fileSource = NULL;
    FILE *fileDestination = NULL;

    char *Source = wide_string_to_UTF8(wcSource);
    char *Destination = NULL;
    if (Source)
    {
        wcfopen(fileSource, Source, "rb");
        FREE(Source);
        Source = NULL;
    }

    Destination = wide_string_to_UTF8(wcDestination);
    if (Destination)
    {
        wcfopen(fileDestination, Destination, "wb");
        FREE(Destination);
        Destination = NULL;
    }

    if (fileDestination == NULL)
    {
        return FILE_OPEN_WRITE_ERROR;
    }

    if (fileSource == NULL)
    {
        return FILE_OPEN_WRITE_ERROR;
    }

    iErr = zlibcompressfile(fileSource, fileDestination, level);

    if (fileSource)
    {
        fclose(fileSource);
        fileSource = NULL;
    }

    if (fileDestination)
    {
        fclose(fileDestination);
        fileDestination = NULL;
    }

    return iErr;
}
/* ==================================================================== */
int zlibdecompressfilename(const wchar_t *wcSource, const wchar_t *wcDestination)
{
    int iErr = Z_OK;
    FILE *fileSource = NULL;
    FILE *fileDestination = NULL;

    char *Source = wide_string_to_UTF8(wcSource);
    char *Destination = NULL;

    if (Source)
    {
        wcfopen(fileSource, Source, "rb");
        FREE(Source);
        Source = NULL;
    }

    Destination = wide_string_to_UTF8(wcDestination);
    if (Destination)
    {
        wcfopen(fileDestination, Destination, "wb");
        FREE(Destination);
        Destination = NULL;
    }

    if (fileDestination == NULL)
    {
        return FILE_OPEN_WRITE_ERROR;
    }

    if (fileSource == NULL)
    {
        return FILE_OPEN_WRITE_ERROR;
    }

    iErr = zlibdecompressfile(fileSource, fileDestination);

    if (fileSource)
    {
        fclose(fileSource);
        fileSource = NULL;
    }

    if (fileDestination)
    {
        fclose(fileDestination);
        fileDestination = NULL;
    }

    return iErr;
}
/* ==================================================================== */
char *zlibcompress(const char *strToCompress, int level, int *iErr)
{
    char *compressedString = NULL;
    Byte *input = (Byte *)strToCompress;
    Byte *output = NULL;
    int err = Z_OK;
    int length = (int) strlen(strToCompress);
    z_stream zst;
    zst.avail_out = length + length/1000 + 12 + 1;
    output = (Byte *)CALLOC(zst.avail_out, sizeof(Byte));
    if (output)
    {
        zst.zalloc = (alloc_func)NULL;
        zst.zfree = (free_func)Z_NULL;
        zst.next_out = (Byte *)output;
        zst.next_in = (Byte *)input;
        zst.avail_in = length;
        err = deflateInit(&zst, level);
        if (err != Z_OK)
        {
            *iErr = err;
            FREE(output);
            output = NULL;

            deflateEnd(&zst);

            return NULL;
        }

        err = deflate(&zst, Z_FINISH);
        if (err != Z_STREAM_END)
        {
            *iErr = err;
            FREE(output);
            output = NULL;

            deflateEnd(&zst);

            return NULL;
        }

        deflateEnd(&zst);

        compressedString = (char *)output;
    }
    else
    {
        *iErr = Z_MEM_ERROR;
    }
    return compressedString;
}
/* ==================================================================== */
char *zlibdecompress(const char *strToDecompress, int *iErr)
{
    #define ratio 1000;
    char *decompressedString = NULL;
    Byte *input = (Byte *)strToDecompress;
    Byte *output = NULL;
    int err = Z_OK ;
    int length = (int) strlen(strToDecompress);
    z_stream zst;
    zst.avail_out = length * ratio;
    output = (Byte *)CALLOC(zst.avail_out, sizeof(Byte));
    if (output)
    {
        zst.zalloc = (alloc_func)NULL;
        zst.zfree = (free_func)Z_NULL;
        zst.next_out = (Byte *)output;
        zst.next_in = (Byte *)input;
        zst.avail_in = length;
        err = inflateInit(&zst);
        if (err != Z_OK)
        {
            *iErr = err;
            FREE(output);
            output = NULL;

            inflateEnd(&zst);

            return NULL;
        }

        err = inflate(&zst, Z_FINISH);

        if ((err != Z_STREAM_END) && (err !=  Z_BUF_ERROR))
        {
            *iErr = err;
            FREE(output);
            output = NULL;

            inflateEnd(&zst);

            return NULL;
        }

        inflateEnd(&zst);

        decompressedString = (char *)output;
    }
    else
    {
        *iErr = Z_MEM_ERROR;
    }
    return decompressedString;
}
/* ==================================================================== */
