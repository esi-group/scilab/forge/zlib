// Allan CORNET - 2011

function builder_c()

  src_c_path = get_absolute_file_path("builder_c.sce");
  CFLAGS = '';
  LDFLAGS = '';


  files_c = "zlib_helpers.c";

  if getos() == "Windows" then 
	files_c = [files_c, "dllMain.c"];
  end

  if getos() <> "Windows" then
    CFLAGS = "-I/usr/include/libz/ -I" + src_c_path;
  else
    
    ARCH_PATH = '';
	if win64() then
		ARCH_PATH = "/../../thirdparty/zlib/windows/win64/include/";
	else
		ARCH_PATH = "/../../thirdparty/zlib/windows/win32/include/";
	end
    CFLAGS = "-I""" + pathconvert(fullpath(src_c_path + ARCH_PATH),%f) + """" + ..
             " -I""" + pathconvert(fullpath(src_c_path),%f) + """";
  end

  tbx_build_src(["c_zlib"],         ..
                files_c,            ..
                "c",                ..
                src_c_path,         ..
                "",                 ..
                LDFLAGS,            ..
                CFLAGS);
  CFLAGS = ilib_include_flag(src_c_path);                
endfunction

builder_c();
clear builder_c; // remove builder_c on stack
