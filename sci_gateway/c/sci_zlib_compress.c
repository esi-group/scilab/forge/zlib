/* ==================================================================== */
/* Allan CORNET - 2011 */
/* ==================================================================== */
#include <stdio.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "MALLOC.h"
#include "charEncoding.h"
#include "zlib_helpers.h"
#include "freeArrayOfString.h"
/* ==================================================================== */
int sci_zlib_compress(char *fname)
{
    SciErr sciErr;
    char **sources = NULL;
    char **compressedStrings = NULL;
    int levelCompression = 1;
    int iErr = 0;
    int m1 = 0, n1 = 0;
    int i = 0;

    CheckRhs(1, 2);
    CheckLhs(1, 1);

    if (Rhs == 2)
    {
        int *piAddressVarTwo = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isScalar(pvApiCtx, piAddressVarTwo))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 2);
            return 0;
        }

        if (isDoubleType(pvApiCtx, piAddressVarTwo))
        {
            double dValueLevel = 0.;
            if (getScalarDouble(pvApiCtx,piAddressVarTwo, &dValueLevel) != 0)
            {
                Scierror(999,_("%s: Memory allocation error.\n"), fname);
                return 0;
            }
            levelCompression = (int)dValueLevel;

            if ((levelCompression < 1) || (levelCompression > 9))
            {
                Scierror(999,_("%s: invalid compression level (1 to 9): %d."), fname, levelCompression);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 2);
            return 0;
        }
    }

    if (Rhs >= 1)
    {
        int *piAddressVarOne = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isStringType(pvApiCtx,piAddressVarOne))
        {
            if (getAllocatedMatrixOfString(pvApiCtx, piAddressVarOne, &m1, &n1, &sources))
            {
                Scierror(999,_("%s: Memory allocation error.\n"), fname);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 1);
            return 0;
        }
    }

    compressedStrings = (char**)MALLOC(sizeof(char*) * (m1 * n1));
    if (compressedStrings == NULL)
    {
        freeAllocatedMatrixOfString(m1, n1, sources);
        sources = NULL;
        Scierror(999,_("%s: Memory allocation error.\n"), fname);
        return 0;
    }

    for (i = 0; i < m1 * n1; i++)
    {
        compressedStrings[i] = zlibcompress(sources[i], levelCompression, &iErr);
        if (iErr != 0) break;
    }

    if (iErr != 0)
    {
        char *msgerr = zlibmessagerror(iErr);
        if (msgerr)
        {
            Scierror(999,_("%s: %s.\n"), fname, msgerr);
            FREE(msgerr);
            msgerr = NULL;
        }
    }
    else 
    {
        sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, m1, n1, compressedStrings);
        if (sciErr.iErr == 0)
        {
            LhsVar(1) = Rhs + 1;
            PutLhsVar();
        }
        else
        {
            printError(&sciErr, 0);
        }
    }

    freeAllocatedMatrixOfString(m1, n1, sources);
    sources = NULL;

    freeArrayOfString(compressedStrings, m1 * n1);
    compressedStrings = NULL;

    return 0;
}
/* ==================================================================== */
