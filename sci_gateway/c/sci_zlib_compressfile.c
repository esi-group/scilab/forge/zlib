/* ==================================================================== */
/* Allan CORNET - 2011 */
/* ==================================================================== */
#include <stdio.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "MALLOC.h"
#include "charEncoding.h"
#include "zlib_helpers.h"
/* ==================================================================== */
int sci_zlib_compressfile(char *fname)
{
    SciErr sciErr;
    wchar_t * wcDestination = NULL;
    wchar_t * wcSource = NULL;
    int levelCompression = 6;
    int iErr = 0;

    CheckRhs(2, 3);
    CheckLhs(1, 1);

    if (Rhs == 3)
    {
        int *piAddressVarThree = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isScalar(pvApiCtx, piAddressVarThree))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 3);
            return 0;
        }

        if (isDoubleType(pvApiCtx, piAddressVarThree))
        {
            double dValueLevel = 0.;
            if (getScalarDouble(pvApiCtx, piAddressVarThree, &dValueLevel) != 0)
            {
                Scierror(999,_("%s: Memory allocation error.\n"), fname);
                return 0;
            }
            levelCompression = (int)dValueLevel;

            if ((levelCompression < 1) || (levelCompression > 9))
            {
                Scierror(999,_("%s: invalid compression level (1 to 9): %d."), fname, levelCompression);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 3);
            return 0;
        }
    }

    if (Rhs >= 2)
    {
        int *piAddressVarTwo = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isScalar(pvApiCtx, piAddressVarTwo))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 2);
            return 0;
        }

        if (isStringType(pvApiCtx,piAddressVarTwo))
        {
            if (getAllocatedSingleWideString(pvApiCtx, piAddressVarTwo, &wcDestination) != 0)
            {
                Scierror(999,_("%s: Memory allocation error.\n"), fname);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 2);
            return 0;
        }
    }

    if (Rhs >= 1)
    {
        int *piAddressVarOne = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
        if(sciErr.iErr)
        {
            if (wcDestination) {FREE(wcDestination); wcDestination = NULL;}
            printError(&sciErr, 0);
            return 0;
        }

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            if (wcDestination) {FREE(wcDestination); wcDestination = NULL;}
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 1);
            return 0;
        }

        if (isStringType(pvApiCtx,piAddressVarOne))
        {
            if (getAllocatedSingleWideString(pvApiCtx, piAddressVarOne, &wcSource) != 0)
            {
                if (wcDestination) {FREE(wcDestination); wcDestination = NULL;}
                Scierror(999,_("%s: Memory allocation error.\n"), fname);
                return 0;
            }
        }
        else
        {
            if (wcDestination) {FREE(wcDestination); wcDestination = NULL;}
            Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 1);
            return 0;
        }
    }

    iErr = zlibcompressfilename(wcSource, wcDestination, levelCompression);
    if (iErr != 0)
    {
        char *msgerr = zlibmessagerror(iErr);
        if (msgerr)
        {
            Scierror(999,_("%s: %s.\n"), fname, msgerr);
            FREE(msgerr);
            msgerr = NULL;
        }
    }
    else 
    {
        LhsVar(1) = 0;
        PutLhsVar();
    }

    FREE(wcSource);
    wcSource = NULL;

    FREE(wcDestination);
    wcDestination = NULL;

    return 0;
}
/* ==================================================================== */
