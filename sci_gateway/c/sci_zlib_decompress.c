/* ==================================================================== */
/* Allan CORNET - 2011 */
/* ==================================================================== */
#include <stdio.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "MALLOC.h"
#include "charEncoding.h"
#include "zlib_helpers.h"
#include "freeArrayOfString.h"
/* ==================================================================== */
int sci_zlib_decompress(char *fname)
{
    SciErr sciErr;
    char **sources = NULL;
    char **decompressedStrings = NULL;
    int levelCompression = 1;
    int iErr = 0;
    int m1 = 0, n1 = 0;
    int i = 0;

    CheckRhs(1, 1);
    CheckLhs(1, 1);

    if (Rhs == 1)
    {
        int *piAddressVarOne = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isStringType(pvApiCtx,piAddressVarOne))
        {
            if (getAllocatedMatrixOfString(pvApiCtx, piAddressVarOne, &m1, &n1, &sources) != 0)
            {
                Scierror(999,_("%s: Memory allocation error.\n"), fname);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 1);
            return 0;
        }
    }

    decompressedStrings = (char**)MALLOC(sizeof(char*) * (m1 * n1));
    if (decompressedStrings == NULL)
    {
        freeAllocatedMatrixOfString(m1, n1, sources);
        sources = NULL;
        Scierror(999,_("%s: Memory allocation error.\n"), fname);
        return 0;
    }

    for(i = 0; i < m1 * n1; i++)
    {
        decompressedStrings[i] = zlibdecompress(sources[i], &iErr);
        if (iErr != 0) break;
    }

    if (iErr != 0)
    {
        char *msgerr = zlibmessagerror(iErr);
        if (msgerr)
        {
            Scierror(999,_("%s: %s.\n"), fname, msgerr);
            FREE(msgerr);
            msgerr = NULL;
        }
    }
    else 
    {
        sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, m1, n1, decompressedStrings);
        if (sciErr.iErr == 0)
        {
            LhsVar(1) = Rhs + 1;
            PutLhsVar();
        }
        else
        {
            printError(&sciErr, 0);
        }
    }

    freeAllocatedMatrixOfString(m1, n1, sources);
    sources = NULL;

    freeArrayOfString(decompressedStrings, m1 * n1);
    decompressedStrings = NULL;

    return 0;
}
/* ==================================================================== */
