//==============================================================================
// Allan CORNET - 2011
//==============================================================================
ref = "Allan CORNET - 2011 Test zlib";
c = zlib_compress(ref, 1);
d = zlib_decompress(c);
assert_checkequal(d, ref);
//==============================================================================
zlib_compressfile(SCI + "/etc/scilab.start", TMPDIR + "/scilab.start.gz", 1);
assert_checkequal(isfile(TMPDIR + "/scilab.start.gz"), %t);
zlib_decompressfile(TMPDIR + "/scilab.start.gz", TMPDIR + "/scilab.start");
ref = mgetl(SCI + "/etc/scilab.start");
r = mgetl(TMPDIR + "/scilab.start");
assert_checkequal(r, ref);
//==============================================================================
