zlib module for Scilab

* functions
zlib_compress(string[, level])
zlib_compressfile(src, dest[,level])
zlib_decompress(string)
zlib_decompressfile(src, dest)

Allan CORNET - 2011
