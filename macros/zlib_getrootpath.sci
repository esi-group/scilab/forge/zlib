// Allan CORNET - 2011
function p = url_getRootPath()
   [fs, p] = libraryinfo("zliblib");
   p = pathconvert(fullpath(p + "../"), %t, %t);
endfunction
